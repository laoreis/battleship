using System.Collections.Generic;

namespace Battleship.Domain.Entities
{
    public class Board
    {
        public List<Position> Positions { get; }

        public Board(int size)
        {
            Positions = new List<Position>();
            size++;

            for (var i = 1; i <= size; i++)
            {
                for (var j = 1; j <= size; j++)
                {
                    Positions.Add(new Position(i, j));
                }
            }
        }
    }
}