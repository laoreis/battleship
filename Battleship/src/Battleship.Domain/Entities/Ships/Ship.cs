namespace Battleship.Domain.Entities.Ships
{
    public class Ship
    {
        public string Name { get; set; }

        public ushort Width { get; set; }
    }
}