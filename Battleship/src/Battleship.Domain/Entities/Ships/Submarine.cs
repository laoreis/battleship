namespace Battleship.Domain.Entities.Ships
{
    public class Submarine : Ship
    {
        public Submarine()
        {
            Name = "Submarine";
            Width = 3;
        }
    }
}