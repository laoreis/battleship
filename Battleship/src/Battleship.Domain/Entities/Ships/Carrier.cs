namespace Battleship.Domain.Entities.Ships
{
    public class Carrier : Ship
    {
        public Carrier()
        {
            Name = "Carrier";
            Width = 5;
        }
    }
}