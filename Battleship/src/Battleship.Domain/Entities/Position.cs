namespace Battleship.Domain.Entities
{
    public class Position
    {
        public Coordinates Coordinates { get; set; }

        public bool IsOccupied { get; set; }

        public Position(int row, int column)
        {
            Coordinates = new Coordinates(row, column);
            IsOccupied = false;
        }
    }
}