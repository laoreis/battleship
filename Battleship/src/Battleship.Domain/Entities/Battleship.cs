namespace Battleship.Domain.Entities
{
    public class Battleship
    {
        public string ShipName { get; set; }

        public string Orientation { get; set; }

        public Coordinates Coordinates { get; set; }
    }
}