using System.Collections.Generic;
using System.Linq;
using Battleship.Domain.Entities;

namespace Battleship.Domain.Extensions
{
    public static class PositionExtensions
    {
        public static Position Exists(this IEnumerable<Position> panels, int row, int column)
        {
            return panels.FirstOrDefault(p => p.Coordinates.Row == row && p.Coordinates.Column == column);
        }

        public static IEnumerable<Position> Area(this IEnumerable<Position> panels, int startRow, int startColumn,
            int endRow,
            int endColumn)
        {
            return panels.Where(p => p.Coordinates.Row >= startRow
                                     && p.Coordinates.Column >= startColumn
                                     && p.Coordinates.Row <= endRow
                                     && p.Coordinates.Column <= endColumn).ToList();
        }

        public static bool HasShips(this IEnumerable<Position> panels)
        {
            return panels.Any(p => p.IsOccupied);
        }
    }
}