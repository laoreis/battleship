using System;
using Battleship.Core.Resources;

namespace Battleship.Domain.Exceptions
{
    public class NotFoundException : Exception
    {
        public NotFoundException(string name)
            : base($"{name} {Messages.NOT_FOUND}")
        {
        }
    }
}