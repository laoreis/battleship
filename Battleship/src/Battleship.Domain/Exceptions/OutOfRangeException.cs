using System;
using Battleship.Core.Resources;

namespace Battleship.Domain.Exceptions
{
    public class OutOfRangeException : Exception
    {
        public OutOfRangeException(string name)
            : base($"{name} {Messages.OUT_OF_RANGE}")
        {
        }
    }
}