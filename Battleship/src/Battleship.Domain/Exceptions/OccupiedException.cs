using System;
using Battleship.Core.Resources;

namespace Battleship.Domain.Exceptions
{
    public class OccupiedException : Exception
    {
        public OccupiedException()
            : base(Messages.POSITION_IS_OCCUPIED)
        {
        }
    }
}