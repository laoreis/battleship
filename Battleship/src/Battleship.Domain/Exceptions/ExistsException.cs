using System;
using Battleship.Core.Resources;

namespace Battleship.Domain.Exceptions
{
    public class ExistsException : Exception
    {
        public ExistsException(string name)
            : base($"{name} {Messages.ALREADY_EXISTS}")
        {
        }
    }
}