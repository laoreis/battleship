using System;
using Battleship.Core.Resources;

namespace Battleship.Domain.Exceptions
{
    public class InvalidException : Exception
    {
        public InvalidException(string name)
            : base($"{name} {Messages.INVALID}")
        {
        }
    }
}