using Battleship.Domain.Entities.Ships;

namespace Battleship.Domain.Interfaces.Factory
{
    public interface IShipFactory
    {
        Ship GetShipByName(string shipName);
    }
}