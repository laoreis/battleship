using Battleship.Domain.Entities;

namespace Battleship.Domain.Interfaces.Service
{
    public interface IBoardService
    {
        string CreateBoard();

        Board GetBoard();
    }
}