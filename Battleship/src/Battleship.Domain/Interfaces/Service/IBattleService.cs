using Battleship.Domain.DTO.Requests;

namespace Battleship.Domain.Interfaces.Service
{
    public interface IBattleService
    {
        string Shot(ShotRequest shotRequest);
    }
}