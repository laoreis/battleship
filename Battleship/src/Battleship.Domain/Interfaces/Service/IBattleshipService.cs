using Battleship.Domain.DTO.Requests;

namespace Battleship.Domain.Interfaces.Service
{
    public interface IBattleshipService
    {
        string Place(BattleshipRequest battleshipRequest);
    }
}