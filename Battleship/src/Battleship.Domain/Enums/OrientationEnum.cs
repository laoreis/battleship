namespace Battleship.Domain.Enums
{
    public enum OrientationEnum
    {
        Horizontal,
        Vertical
    }
}