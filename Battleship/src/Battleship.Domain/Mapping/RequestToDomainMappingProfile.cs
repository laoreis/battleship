using AutoMapper;
using Battleship.Domain.DTO.Requests;
using Battleship.Domain.Entities;

namespace Battleship.Domain.Mapping
{
    public class RequestToDomainMappingProfile : Profile
    {
        public RequestToDomainMappingProfile()
        {
            CreateMap<BattleshipRequest, Domain.Entities.Battleship>()
                .ForMember(dest => dest.Coordinates,
                    opt => opt.MapFrom(src => new Coordinates(src.Row, src.Column)));
            CreateMap<ShotRequest, Coordinates>();
            
        }
    }
}