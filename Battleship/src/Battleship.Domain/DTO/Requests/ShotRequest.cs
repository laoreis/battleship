namespace Battleship.Domain.DTO.Requests
{
    public class ShotRequest
    {
        public int Row { get; set; }

        public int Column { get; set; }
    }
}