namespace Battleship.Domain.DTO.Requests
{
    public class BattleshipRequest
    {
        public string ShipName { get; set; }

        public string Orientation { get; set; }

        public int Row { get; set; }

        public int Column { get; set; }
    }
}