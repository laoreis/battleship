using Battleship.Domain.DTO.Requests;
using Battleship.Domain.Interfaces.Service;
using Microsoft.AspNetCore.Mvc;

namespace Battleship.API.Controllers
{
    [Route("api/[controller]")]
    public class BattleshipController : ControllerBase
    {
        private readonly IBattleshipService _battleshipService;

        public BattleshipController(IBattleshipService battleshipService)
        {
            _battleshipService = battleshipService;
        }

        [HttpPost]
        public IActionResult Place([FromBody] BattleshipRequest battleshipRequest)
        {
            if (battleshipRequest == null)
            {
                return BadRequest();
            }

            var response = _battleshipService.Place(battleshipRequest);
            return Ok(response);
        }
    }
}