using Battleship.Domain.DTO.Requests;
using Battleship.Domain.Interfaces.Service;
using Microsoft.AspNetCore.Mvc;

namespace Battleship.API.Controllers
{
    [Route("api/[controller]")]
    public class BattleController : ControllerBase
    {
        private readonly IBattleService _battleService;

        public BattleController(IBattleService battleService)
        {
            _battleService = battleService;
        }

        [HttpPost]
        public IActionResult Shot([FromBody] ShotRequest shotRequest)
        {
            if (shotRequest == null)
            {
                return BadRequest();
            }

            var response = _battleService.Shot(shotRequest);
            return Ok(response);
        }
    }
}