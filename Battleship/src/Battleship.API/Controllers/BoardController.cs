using Battleship.Domain.Interfaces.Service;
using Microsoft.AspNetCore.Mvc;

namespace Battleship.API.Controllers
{
    [Route("api/[controller]")]
    public class BoardController : ControllerBase
    {
        private readonly IBoardService _boardService;

        public BoardController(IBoardService boardService)
        {
            _boardService = boardService;
        }

        [HttpPost]
        public IActionResult Create()
        {
            var response = _boardService.CreateBoard();
            return Ok(response);
        }
    }
}