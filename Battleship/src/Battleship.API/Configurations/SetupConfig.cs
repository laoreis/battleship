using AutoMapper;
using Battleship.Application.Factory;
using Battleship.Application.Services;
using Battleship.Domain.Entities.Ships;
using Battleship.Domain.Interfaces.Factory;
using Battleship.Domain.Interfaces.Service;
using Battleship.Domain.Mapping;
using FluentValidation.AspNetCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.OpenApi.Models;

namespace Battleship.API.Configurations
{
    public static class SetupConfig
    {
        public static void AddCustomSwagger(this IServiceCollection services)
        {
            services.AddSwaggerGen(s =>
            {
                s.SwaggerDoc("v1", new OpenApiInfo
                {
                    Version = "v1",
                    Title = "Battleship - Lao Reis",
                    Description = "Battleship - Lao Reis",
                    Contact = new OpenApiContact
                    {
                        Name = "Lao Reis",
                        Email = "lbrsleite@gmail.com"
                    }
                });
            });
        }

        public static void AddCustomAutoMapper(this IServiceCollection services)
        {
            var mappingConfig = new MapperConfiguration(mc =>
            {
                mc.AddProfile(new RequestToDomainMappingProfile());
            });

            var mapper = mappingConfig.CreateMapper();
            services.AddSingleton(mapper);
        }

        public static void AddDependencyInjection(this IServiceCollection services)
        {
            // Services
            services.AddSingleton<IBoardService, BoardService>();
            services.AddSingleton<IBattleshipService, BattleshipService>();
            services.AddSingleton<IBattleService, BattleService>();

            // Factory
            services.AddSingleton<IShipFactory, ShipFactory>();
            services.AddSingleton<Ship, Carrier>();
            services.AddSingleton<Ship, Cruiser>();
            services.AddSingleton<Ship, Destroyer>();
            services.AddSingleton<Ship, Submarine>();
         
        }
        
        public static void AddFluentValidator(this IServiceCollection services)
        {
            services.AddMvcCore()
                .AddFluentValidation(fluent => fluent.RegisterValidatorsFromAssemblyContaining<Startup>());
        }
    }
}