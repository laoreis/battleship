using Battleship.Core.Resources;
using Battleship.Domain.DTO.Requests;
using FluentValidation;

namespace Battleship.Application.Common.Validators
{
    public class BattleshipRequestValidator : AbstractValidator<BattleshipRequest> 
    {
        public BattleshipRequestValidator()
        {
            RuleFor(x => x.ShipName)
                .NotEmpty()
                .WithMessage($"Ship {Messages.FIELD_REQUIRED}");

            RuleFor(x => x.Row)
                .GreaterThan(0).WithMessage($"Row {Messages.GREATER_THAN_ZERO}")
                .LessThan(10).WithMessage($"Row {Messages.LESS_THAN_ZERO}");

            RuleFor(x => x.Column)
                .GreaterThan(0).WithMessage($"Column {Messages.GREATER_THAN_ZERO}")
                .LessThan(10).WithMessage($"Column {Messages.LESS_THAN_ZERO}");

            RuleFor(x => x.Orientation)
                .NotEmpty().WithMessage($"Orientation {Messages.FIELD_REQUIRED}");
        }
    }
}