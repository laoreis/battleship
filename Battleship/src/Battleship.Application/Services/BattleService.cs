using AutoMapper;
using Battleship.Application.Common.Validators;
using Battleship.Core.Resources;
using Battleship.Domain.DTO.Requests;
using Battleship.Domain.Entities;
using Battleship.Domain.Exceptions;
using Battleship.Domain.Extensions;
using Battleship.Domain.Interfaces.Service;
using FluentValidation;

namespace Battleship.Application.Services
{
    public class BattleService : IBattleService
    {
        private readonly IBoardService _boardService;

        private readonly IMapper _mapper;

        public BattleService(IBoardService boardService, IMapper mapper)
        {
            _boardService = boardService;
            _mapper = mapper;
        }

        public string Shot(ShotRequest shotRequest)
        {
            ValidateBattle(shotRequest);

            var coordinates = _mapper.Map<Coordinates>(shotRequest);
            var board = _boardService.GetBoard();
            var position = board.Positions.Exists(coordinates.Row, coordinates.Column);

            if (position == null)
            {
                throw new NotFoundException("Position");
            }

            var hasShips = board.Positions.HasShips();

            if (hasShips == false)
            {
                throw new NotFoundException("Ships");
            }

            return position.IsOccupied ? Messages.HIT : Messages.MISS;
        }

        private void ValidateBattle(ShotRequest shotRequest)
        {
            var validator = new ShotRequestValidator();
            var result = validator.Validate(shotRequest);

            var failures = result.Errors;

            if (failures.Count != 0)
            {
                throw new ValidationException(failures);
            }
        }
    }
}