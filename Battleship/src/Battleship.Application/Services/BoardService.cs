using Battleship.Core.Resources;
using Battleship.Domain.Entities;
using Battleship.Domain.Exceptions;
using Battleship.Domain.Interfaces.Service;

namespace Battleship.Application.Services
{
    public class BoardService : IBoardService
    {
        private Board Board { get; set; }

        public string CreateBoard()
        {
            if (Board != null)
            {
                throw new ExistsException("Board");
            }

            Board = new Board(10);

            return Messages.BOARD_CREATED_SUCCESSFULLY;
        }

        public Board GetBoard()
        {
            if (Board == null)
            {
                throw new NotFoundException("Board");
            }

            return Board;
        }
    }
}