using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using Battleship.Application.Common.Validators;
using Battleship.Core.Resources;
using Battleship.Domain.DTO.Requests;
using Battleship.Domain.Entities;
using Battleship.Domain.Enums;
using Battleship.Domain.Exceptions;
using Battleship.Domain.Extensions;
using Battleship.Domain.Interfaces.Factory;
using Battleship.Domain.Interfaces.Service;
using FluentValidation;

namespace Battleship.Application.Services
{
    public class BattleshipService : IBattleshipService
    {
        private readonly IBoardService _boardService;
        private readonly IShipFactory _shipFactory;
        private readonly IMapper _mapper;

        public BattleshipService(
            IBoardService boardService,
            IShipFactory shipFactory,
            IMapper mapper)
        {
            _boardService = boardService;
            _shipFactory = shipFactory;
            _mapper = mapper;
        }

        public string Place(BattleshipRequest battleshipRequest)
        {
            ValidateBattleship(battleshipRequest);

            var board = _boardService.GetBoard();

            var battleship = _mapper.Map<Domain.Entities.Battleship>(battleshipRequest);

            var ship = _shipFactory.GetShipByName(battleship.ShipName);

            var positionAreas = PositionAreaValidation(battleship, board, ship.Width);

            SetPosition(positionAreas);

            return Messages.SHIP_PLACED_SUCCESSFULLY;
        }

        private void ValidateBattleship(BattleshipRequest battleshipRequest)
        {
            var validator = new BattleshipRequestValidator();
            var result = validator.Validate(battleshipRequest);

            var failures = result.Errors;

            if (failures.Count != 0)
            {
                throw new ValidationException(failures);
            }
        }

        private IEnumerable<Position> PositionAreaValidation(Battleship.Domain.Entities.Battleship battleship,
            Board board, int shipWidth)
        {
            var orientationEnum = GetOrientation(battleship.Orientation);
            var shipCoordinates = new Coordinates(battleship.Coordinates.Row, battleship.Coordinates.Column);

            switch (orientationEnum)
            {
                case OrientationEnum.Horizontal:
                    shipCoordinates.Column = battleship.Coordinates.Column + shipWidth;
                    break;
                case OrientationEnum.Vertical:
                    shipCoordinates.Row = battleship.Coordinates.Row + shipWidth;
                    break;
            }

            var positionAreas = GetPositionAreas(battleship.Coordinates, shipCoordinates, board.Positions);

            return positionAreas;
        }

        private OrientationEnum GetOrientation(string orientation)
        {
            var orientationIsValid = Enum.TryParse(orientation, out OrientationEnum orientationEnum);

            if (orientationIsValid == false)
                throw new InvalidException("Orientation");

            return orientationEnum;
        }

        private IEnumerable<Position> GetPositionAreas(Coordinates coordinates, Coordinates shitCoordinates,
            IEnumerable<Position> positions)
        {
            var positionAreas = positions.Area(
                coordinates.Row,
                coordinates.Column,
                shitCoordinates.Row,
                shitCoordinates.Column);

            return positionAreas;
        }

        private void SetPosition(IEnumerable<Position> positionAreas)
        {
            if (positionAreas.Any() == false)
            {
                throw new OutOfRangeException("Position Area");
            }

            if (positionAreas.Any(pl => pl.IsOccupied))
            {
                throw new OccupiedException();
            }

            foreach (var panel in positionAreas)
            {
                panel.IsOccupied = true;
            }
        }
    }
}