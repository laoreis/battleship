using System.Collections.Generic;
using System.Linq;
using Battleship.Domain.Entities.Ships;
using Battleship.Domain.Exceptions;
using Battleship.Domain.Interfaces.Factory;

namespace Battleship.Application.Factory
{
    public class ShipFactory : IShipFactory
    {
        private readonly IEnumerable<Ship> _ships;

        public ShipFactory(IEnumerable<Ship> ships)
        {
            _ships = ships;
        }

        public Ship GetShipByName(string shipName)
        {
            var ship = _ships.FirstOrDefault(s => s.Name == shipName);

            if (ship == null)
                throw new NotFoundException($"Ship");

            return ship;
        }
    }
}