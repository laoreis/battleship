using System.Net;
using Battleship.API.Controllers;
using Battleship.Core.Resources;
using Battleship.Domain.DTO.Requests;
using Battleship.Domain.Interfaces.Service;
using Microsoft.AspNetCore.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace Battleship.API.Tests.Controller
{
    [TestClass]
    public class BattleshipControllerUnitTests
    {
        private Mock<IBattleshipService> _battleshipServiceMock;
        private BattleshipController _battleshipController;

        [TestInitialize]
        public void Initialise()
        {
            _battleshipServiceMock = new Mock<IBattleshipService>();
        }

        [TestMethod]
        public void Place_ReturnsOk_WhenPlaceABattleship()
        {
            // Arrange
            _battleshipServiceMock.Setup(x => x.Place(It.IsAny<BattleshipRequest>()))
                .Returns(Messages.SHIP_PLACED_SUCCESSFULLY);
            _battleshipController = new BattleshipController(_battleshipServiceMock.Object);

            // Act
            var result = _battleshipController.Place(new BattleshipRequest());

            // Assert
            Assert.IsInstanceOfType(result, typeof(OkObjectResult));
            var okResult = result as OkObjectResult;

            Assert.IsNotNull(okResult);
            Assert.AreEqual((int) HttpStatusCode.OK, okResult.StatusCode);
        }
    }
}