using System.Net;
using Battleship.API.Controllers;
using Battleship.Core.Resources;
using Battleship.Domain.Interfaces.Service;
using Microsoft.AspNetCore.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace Battleship.API.Tests.Controller
{
    [TestClass]
    public class BoardControllerUnitTests
    {
        private Mock<IBoardService> _boardServiceMock;
        private BoardController _boardController;

        [TestInitialize]
        public void Initialise()
        {
            _boardServiceMock = new Mock<IBoardService>();
        }

        [TestMethod]
        public void Create_ReturnsOk_WhenCreateBoard()
        {
            // Arrange
            _boardServiceMock.Setup(x => x.CreateBoard()).Returns(Messages.BOARD_CREATED_SUCCESSFULLY);
            _boardController = new BoardController(_boardServiceMock.Object);

            // Act
            var result = _boardController.Create();
            
            // Assert
            Assert.IsInstanceOfType(result, typeof(OkObjectResult));
            
            var okResult = result as OkObjectResult;
            
            Assert.IsNotNull(okResult);
            Assert.AreEqual((int) HttpStatusCode.OK, okResult.StatusCode);
        }
    }
}