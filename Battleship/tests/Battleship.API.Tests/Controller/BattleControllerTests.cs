using System.Net;
using Battleship.API.Controllers;
using Battleship.Core.Resources;
using Battleship.Domain.DTO.Requests;
using Battleship.Domain.Interfaces.Service;
using Microsoft.AspNetCore.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace Battleship.API.Tests.Controller
{
    [TestClass]
    public class BattleControllerUnitTests
    {
        private Mock<IBattleService> _battleshipMock;
        private BattleController _battleController;

        [TestInitialize]
        public void Initialise()
        {
            _battleshipMock = new Mock<IBattleService>();
        }

        [TestMethod]
        public void Shot_ReturnsOk()
        {
            // Arrange
            _battleshipMock.Setup(x => x.Shot(It.IsAny<ShotRequest>())).Returns(Messages.MISS);
            _battleController = new BattleController(_battleshipMock.Object);

            // Act
            var result = _battleController.Shot(new ShotRequest());

            // Assert
            Assert.IsInstanceOfType(result, typeof(OkObjectResult));
            var okResult = result as OkObjectResult;

            Assert.IsNotNull(okResult);
            Assert.AreEqual((int) HttpStatusCode.OK, okResult.StatusCode);
        }
    }
}