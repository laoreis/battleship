using Battleship.Application.Services;
using Battleship.Core.Resources;
using Battleship.Domain.Exceptions;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Battleship.Core.Tests.Services
{
    [TestClass]
    public class BoardServiceUnitTests
    {
        [TestMethod]
        public void CreateBoard_ReturnsSuccessMessage()
        {
            // Arrange
            var boardService = new BoardService();

            // Act
            var response = boardService.CreateBoard();

            // Assert
            Assert.IsNotNull(response);
            Assert.AreEqual(Messages.BOARD_CREATED_SUCCESSFULLY, response);
        }
        
        [TestMethod]
        [ExpectedException(typeof(ExistsException))]
        public void CreateBoard_ReturnsThrowExitsException_WhenBoardIsAlreadyExists()
        {
            // Arrange
            var boardService = new BoardService();
            
            // Act
            boardService.CreateBoard(); // Created
            boardService.CreateBoard(); // Try to created again
        }
        
        [TestMethod]
        [ExpectedException(typeof(NotFoundException))]
        public void GetBoard_ReturnsThrowNotFoundException_WhenBoardIsNull()
        {
            // Arrange
            var boardService = new BoardService();
            
            // Act
            boardService.GetBoard();
        }
        
        [TestMethod]
        public void GetBoard_ReturnsBoardObject_WhenBoardIsAlreadyCreated()
        {
            // Arrange
            var boardService = new BoardService();
            boardService.CreateBoard();
            
            // Act
            var response = boardService.GetBoard();

            // Assert
            Assert.IsNotNull(response);
        }
    }
}