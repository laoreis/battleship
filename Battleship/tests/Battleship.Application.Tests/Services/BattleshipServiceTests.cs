using AutoMapper;
using Battleship.Application.Services;
using Battleship.Core.Resources;
using Battleship.Domain.DTO.Requests;
using Battleship.Domain.Entities;
using Battleship.Domain.Entities.Ships;
using Battleship.Domain.Exceptions;
using Battleship.Domain.Interfaces.Factory;
using Battleship.Domain.Interfaces.Service;
using FluentValidation;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace Battleship.Core.Tests.Services
{
    [TestClass]
    public class BattleshipServiceUnitTests
    {
        private BattleshipService _battleshipService;
        private Mock<IBoardService> _boardServiceMock;
        private Mock<IShipFactory> _shipFactory;
        private Mock<IMapper> _mapperMock;

        [TestInitialize]
        public void Initialise()
        {
            _boardServiceMock = new Mock<IBoardService>();
            _shipFactory = new Mock<IShipFactory>();
            _mapperMock = new Mock<IMapper>();
        }

        [TestMethod]
        public void Place_ReturnOk_WhenPassAllParametersValid()
        {
            // Arrange
            var battleshipRequest = new BattleshipRequest
            {
                ShipName = "Cruiser",
                Orientation = "Horizontal",
                Row = 1,
                Column = 1
            };
            
            _boardServiceMock.Setup(x => x.GetBoard()).Returns(new Board(10));
            _shipFactory.Setup(x => x.GetShipByName(It.IsAny<string>())).Returns(new Cruiser());
            _mapperMock.Setup(x => x.Map<Domain.Entities.Battleship>(battleshipRequest)).Returns(new Domain.Entities.Battleship
            {
                Coordinates = new Coordinates(1, 1),
                Orientation = "Horizontal",
                ShipName = "Cruiser"
            });
            
            _battleshipService = new BattleshipService(
                _boardServiceMock.Object,
                _shipFactory.Object,
                _mapperMock.Object
            );
            
            // Act
            var response = _battleshipService.Place(battleshipRequest);
            
            // Assert
            Assert.IsNotNull(response);
            Assert.AreEqual(Messages.SHIP_PLACED_SUCCESSFULLY, response);
        }
        
        [TestMethod]
        [ExpectedException(typeof(ValidationException))]
        public void Place_ReturnValidationException_WhenMissAPropertyOnParameter()
        {
            // Arrange
            var battleshipRequest = new BattleshipRequest
            {
                Orientation = "Horizontal",
                Row = 1,
                Column = 1
            };
            
            _battleshipService = new BattleshipService(
                _boardServiceMock.Object,
                _shipFactory.Object,
                _mapperMock.Object
            );
            
            // Act
            _battleshipService.Place(battleshipRequest);
        }
        
        [TestMethod]
        [ExpectedException(typeof(InvalidException))]
        public void Place_ReturnInvalidException_WhenOrientationIsWrong()
        {
            // Arrange
            var orientation = "Test123";
            var battleshipRequest = new BattleshipRequest
            {
                ShipName = "Cruiser",
                Orientation = orientation,
                Row = 1,
                Column = 1
            };
            
            _boardServiceMock.Setup(x => x.GetBoard()).Returns(new Board(10));
            _shipFactory.Setup(x => x.GetShipByName(It.IsAny<string>())).Returns(new Cruiser());
            _mapperMock.Setup(x => x.Map<Domain.Entities.Battleship>(battleshipRequest)).Returns(new Domain.Entities.Battleship
            {
                Coordinates = new Coordinates(1, 1),
                Orientation = orientation,
                ShipName = "Cruiser"
            });
            
            _battleshipService = new BattleshipService(
                _boardServiceMock.Object,
                _shipFactory.Object,
                _mapperMock.Object
            );
            
            // Act
            _battleshipService.Place(battleshipRequest);
        }
    }
}