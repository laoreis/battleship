using System.Linq;
using AutoMapper;
using Battleship.Application.Services;
using Battleship.Core.Resources;
using Battleship.Domain.DTO.Requests;
using Battleship.Domain.Entities;
using Battleship.Domain.Exceptions;
using Battleship.Domain.Interfaces.Service;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace Battleship.Core.Tests.Services
{
    [TestClass]
    public class BattleServiceUnitTests
    {
        private Mock<IBoardService> _boardServiceMock;
        private BattleService _battleService;
        private Mock<IMapper> _mapperMock;

        [TestInitialize]
        public void Initialise()
        {
            _boardServiceMock = new Mock<IBoardService>();
            _mapperMock = new Mock<IMapper>();
            _boardServiceMock = new Mock<IBoardService>();
        }

        [TestMethod]
        [ExpectedException(typeof(NotFoundException))]
        public void Shot_ReturnNotFoundException_WhenThereAreNotShipPlacedOnTheBoard()
        {
            // Arrange
            var shotRequest = new ShotRequest
            {
                Row = 1,
                Column = 1
            };

            _boardServiceMock.Setup(x => x.GetBoard()).Returns(new Board(10));
            _battleService = new BattleService(_boardServiceMock.Object, _mapperMock.Object);
            _mapperMock.Setup(x => x.Map<Coordinates>(shotRequest))
                .Returns(new Coordinates(1, 1));

            // Act
            _battleService.Shot(shotRequest);
        }

        [TestMethod]
        public void Shot_ReturnHit_WhenThePositionIsOccupied()
        {
            // Arrange
            var shotRequest = new ShotRequest
            {
                Row = 1,
                Column = 1
            };

            var board = new Board(10);
            board.Positions.First(p => p.IsOccupied = true);

            _boardServiceMock.Setup(x => x.GetBoard()).Returns(board);
            _battleService = new BattleService(_boardServiceMock.Object, _mapperMock.Object);
            _mapperMock.Setup(x => x.Map<Coordinates>(shotRequest))
                .Returns(new Coordinates(1, 1));
            
            // Act
           var response = _battleService.Shot(shotRequest);
            
            // Assert
            Assert.IsNotNull(response);
            Assert.AreEqual(Messages.HIT, response);
        }
        
        [TestMethod]
        public void Shot_ReturnMiss_WhenThePositionIsOccupied()
        {
            // Arrange
            var shotRequest = new ShotRequest
            {
                Row = 1,
                Column = 1
            };

            var board = new Board(10);
            board.Positions.First(p => p.IsOccupied = true);

            _boardServiceMock.Setup(x => x.GetBoard()).Returns(board);
            _battleService = new BattleService(_boardServiceMock.Object, _mapperMock.Object);
            _mapperMock.Setup(x => x.Map<Coordinates>(shotRequest))
                .Returns(new Coordinates(1, 2));
            
            // Act
            var response = _battleService.Shot(shotRequest);
            
            // Assert
            Assert.IsNotNull(response);
            Assert.AreEqual(Messages.MISS, response);
        }
    }
}