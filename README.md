# Battleship State Tracker
Battleship state tracking API for a <b>Single Player</b>
The API does not support the entire game, just the state tracker.

<b>Actions: </b>
- Create a board
- Add a battleship to the board
- Take a “shot” at a given position, and report back whether the shot
resulted in a hit or a miss.

## Demo
Check the game online [demo](https://battleship-lao-reis.azurewebsites.net/swagger/index.html)

## Requirements
- Visual Studio 2019 [download](https://visualstudio.microsoft.com/vs/)
- Dotnet Core 3.1 [download](https://dotnet.microsoft.com/download/dotnet-core/3.1)

## Usage

There are 3 methods

- 1 - Create a board: <b>board/create</b>
	It is a method <b>POST</b> without paramenters
 
- 2 - Place a Battleship: <b>battleship/place</b>
	It is a method <b>POST</b> with <b>4</b> parameters
	```json
		{
		  "shipName": "string",
		  "orientation": "string",
		  "row": 0,
		  "column": 0
		}
	```
	- <b>shipName</b> represents the ships' name: <b>Carrier, Cruiser, Destroyer and Submarine</b>;
	- <b>orientation</b> represents the orientation that ship will be placed on the board: <b>Horizontal or Vertical</b>;
	- <b>row</b> represents the board position <b>horizontally</b>. 
		- Values are between: <b>Min: 1 Max: 10</b>;
	 - <b>column</b> represents the board position <b>vertically</b>.  
		 - Values are between: <b>Min: 1 Max: 10</b>;

- 3 - Shot: <b>battle/shot</b>
	It is a method <b>POST</b> with <b>2</b> parameters, and the return will be: <b>Hit or Miss</b>
	```json
		{
		  "row": 0,
		  "column": 0
		}
	```	
	- <b>row</b> represents the board position <b>horizontally</b>. 
		- Values are between: <b>Min: 1 Max: 10</b>;
	 - <b>column</b> represents the board position <b>vertically</b>.  
		 - Values are between: <b>Min: 1 Max: 10</b>;
	
## Roadmap

### Version 0.0.1
- :white_check_mark: 1.1 - Create a board
- :white_check_mark: 1.2 - Add a battleship to the board
- :white_check_mark: 1.3 - Take an “shot” at a given position, and report back whether the shot resulted in a hit or a miss.
- :white_check_mark: 1.4 - Add Swagger to manage the requests
- :white_check_mark: 1.5 - Create unit tests to test a few methods
- :white_check_mark: 1.6 - Create filter to manage all errors
- :white_check_mark: 1.7 - Create factory to manage the ships' instances
 
### Version 0.0.2
- 2.1 - Improvement logic to manage the ships
- 2.2 - Add unit tests to cover all the methods
- 2.3 - Add a generic class to manage all response messages
- 2.4 - Add an option to user provide the Board size (Row x Column)

### License
Battleship is [MIT licensed](./LICENSE).
